/*

	Playlist Copier

	Copyright (c) 2016, Logan Matthew Nichols

	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
	REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
	INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
	LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
	OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
	PERFORMANCE OF THIS SOFTWARE.

	This software utilizes FFmpeg.
	FFmpeg copyright (c) 2000 - 2016 the FFmpeg developers

*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

void copy (string src, string dst);
void flac2mp3 (const string& in, const string& out, const string& bitrate);
void aac2mp3 (const string& in, const string& out, const string& bitrate);
inline bool isFile (const string& name);

int main() {

	ifstream fileIn;
	ofstream fileOut;
	string playlistPath = "/mnt/music/playlist/casual.m3u";
	string dirPath = "/mnt/music/playlist/casual";
	string filePath = "/mnt/music/files";
	string bitrate_flac = "320";
	string bitrate_aac = "256";
	string overwrite = "y";
	string next, currPath, currExt, currFile, targetFile, convFile;
	vector<string> tracks, exts;
	bool fail;
	int mp3 = 0, flac = 0, aac = 0, other = 0;
	double progress;

	fileIn.open("/mnt/music/playlist/casual.m3u");
	if (fileIn.fail()) {
		cout << "[main] - error opening playlist file" << endl;
		return 0;
	}

	/*do {
		cout << "Path to playlist file (.m3u or .m3u8): ";
		getline(cin, playlistPath);
		if (!playlistPath.find_first_of("\""))
			// check for quotation marks around the filepath (sees if first character
			// is a '"')
			playlistPath = playlistPath.substr(playlistPath.find_first_not_of("\""),
				playlistPath.find_last_not_of("\"")); //remove quotation marks
		fileIn.open(playlistPath.c_str());
		fail = fileIn.fail();
		if (playlistPath.substr((playlistPath.size() - 4), playlistPath.size()) != ".m3u" &&
			playlistPath.substr((playlistPath.size() - 5), playlistPath.size()) != ".m3u8")
			fail = true;
		if (fail)
			cout << "[main] - unable to open playlist file..." << endl;
	} while (fail);*/

	cout << "[main] - reading playlist file...";
	
	//int test = atoi("1");	

	while (getline(fileIn, next)) {
		if (next.substr(0,1) != "#") { //make sure comments aren't read in
			replace(next.begin(), next.end(), '\\', '/');
			//tracks.push_back(next.substr(0, next.find_last_of(".")));
			cout << next << endl;
			string temp = next.substr(0, next.find_last_of("."));
			temp = temp.substr(0, temp.find_last_of('/'));
			temp = temp.substr(0, temp.find_last_of('/'));
			temp = temp.substr(0, temp.find_last_of('/'));
			string temp2 = next.substr(temp.length(), next.length());
			temp2 = temp2.substr(0, temp2.find_last_of("."));
			tracks.push_back(temp2);
			cout << "\t"
				<< temp2
				<< endl;
			currExt = next.substr(next.find_last_of("."));
			cout << "\t" << currExt << endl;
			//cout << "\t.mp3" << "==" << currExt << ": ";
			//cout << ((currExt == ".mp3") ? "true" : "false") << endl;
			if (currExt == ".mp3" || currExt == ".MP3") {
				mp3++;
				exts.push_back("mp3");
				//cout << "-> mp3" << endl;
			} else if (currExt == ".flac" || currExt == ".FLAC") {
				exts.push_back("flac");
				flac++;
				//cout << "-> flac" << endl;
			} else if (currExt == ".m4a" || currExt == ".M4A") {
				aac++;
				exts.push_back("m4a");
				//cout << "-> m4a" << endl;
			} else {
				other++;
				exts.push_back("");
			}
		}
	}

	cout
		<< "done" << endl
		<< "[main] \t Total files \t\t" << tracks.size() << endl
		<< "\t MP3 files:\t\t" << mp3 << endl
		<< "\t FLAC files:\t\t" << flac << endl
		<< "\t AAC files:\t\t" << aac << endl
		<< "\t Unsupported files: \t" << other << endl
	;

	fileIn.close();

	cout << "Path to copy tracks to: ";
	getline(cin, dirPath);

	for (int i = 0; i < (int)tracks.size(); i++) {
		currPath = dirPath + tracks[i].substr(tracks[i].find_last_of("/"));
		currFile = currPath + ".mp3";
		targetFile = filePath + tracks[i] + "." + exts[i];
		convFile = tracks[i] + ".mp3";
		progress = ((double)i / (double)tracks.size()) * 100;
		cout
			<< "< "
			<< progress
			<< "% > - "
			<< tracks[i].substr(tracks[i].find_last_of("/\\") + 1
				, tracks[i].size()) +
				"." + exts[i]
			<< endl;
		if (!isFile(currFile)) {
			if (exts[i] == "flac") {
				flac2mp3(targetFile, currFile, bitrate_flac);
			} else if (exts[i] == "m4a") {
				aac2mp3(targetFile, currFile, bitrate_aac);
			} else if (exts[i] == "mp3") {
				copy(filePath + convFile, currFile);
			} else {
				cout << "Skipping unsupported file:\t" << targetFile << endl;
			}
		} else if (isFile(currFile) && overwrite == "y") {
			if (exts[i] == "flac") {
				remove(currFile.c_str());
				flac2mp3(targetFile, currFile, bitrate_flac);
			} else if (exts[i] == "m4a") {
				remove(currFile.c_str());
				aac2mp3(targetFile, currFile, bitrate_aac);
			} else if (exts[i] == "mp3") {
				remove(currFile.c_str());
				copy(filePath + convFile, currFile);
			} else {
				cout << "Skipping: unsupported file" << endl;
			}
		} else {
			cout << "Skipping: already exists" << endl;
		}
	}

	cout << "\nCompleted\n" << endl;

	return 0;

}

void flac2mp3 (const string& in, const string& out, const string& bitrate) {
	cout << "\t[ffmpeg] - transcoding...";
	system((
		"ffmpeg -loglevel quiet -threads 0 -i \"" +
		in + "\" -ab " + bitrate +
		"k -map_metadata 0 -id3v2_version 3 \"" + out + "\"").c_str());
	cout << "done" << endl;
}

void aac2mp3 (const string& in, const string& out, const string& bitrate) {
	cout << "\t[ffmpeg] - transcoding...";
	system(("ffmpeg -loglevel quiet -threads 0 -i \"" +
	in + "\" -ab " + bitrate +
	"k -map_metadata 0 -id3v2_version 3 \"" + out + "\"").c_str());
	cout << "done" << endl;
}

void copy (const string src, const string dst) {

	ifstream fileIn;
	ofstream fileOut;

	fileIn.open(src.c_str(), ios::binary);
	if (fileIn.fail()) {
		cout << "\t[copy] - unable to open file: " << src << endl;
		return;
	}
	fileOut.open(dst.c_str(), ios::binary);

	cout << "\t[copy] - copying...";
	fileOut << fileIn.rdbuf();
	cout << "done" << endl;

	fileIn.close();
	fileOut.close();

}

inline bool isFile (const string& name) { //not mine either
	bool exists = true;
	ifstream fileIn;
	fileIn.open(name.c_str());
	if (fileIn.fail()) {
		exists = false;
	}
	fileIn.close();
	return exists;
}
